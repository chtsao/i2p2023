---
title: "Final Exam"
date: 2023-12-24T08:33:57+08:00
draft: false
tags: [adm, final]
---
<img src="https://www.animegeek.com/wp-content/uploads/2022/04/SPY-x-FAMILY-Part-2-release-date-in-Fall-2022-SPYxFAMILY-Episode-13-to-25-a-split-cour.jpg" style="zoom: 67%;" />

### Final Exam

* 日期: 2024 0102 (二). 時間: 1310-1440. 
* 地點: A 210
* 範圍：上課及習題內容。對照課本章節約為
  * Chapter 5, Chapter 7; Chapter 8 (Sec 8.3, 8.4 除外)
  * Chapter 9; Chapter 10; Chapter 11 (Sec 11.3 除外). Chapter 13. 
* 其他：No cheatsheet nor mobile phone. Prepare early and Good Luck!

提早準備，固實會的，加強生疏的，弄懂原來不會的！----考試不難，會就簡單！

### 考古題

I2P FINAL(Fixed):  [2021](https://chtsao.gitlab.io/i2p2022/fin2021.pdf), [2022](https://chtsao.gitlab.io/i2p2023/fin2022.pdf)

注意：Chapter 13, Chebyshev's inequality + Law of Large Numbers 也是今年(最後一週要上ㄡ)才上到的內容。但都是考試的重要主題。2020年前的考古題參考價值較低，沒有提供。請自行針對上課筆記、課本習題內容練習。總之是要會，不要背。好好參考[Study less, study smart](https://chtsao.gitlab.io/i2p2022/posts/hello2/#study-less-study-smart)的幾個原則如 Quizz+Space+Mix. 加油！

* <非營利廣告> 讀累了，休息一下，聽聽 [YesKno 時間](https://open.spotify.com/show/1KlWNRx9QIu7m7JpqlIBhL?si=a2caedcb86944fa7) 再衝 :fire: :fire: :fire:！ 