---
title: "Asymptotic Theorems on X-bar"
date: 2024-01-09T04:23:17+08:00
draft: false
categories: [adm, notes]
tags: [Law of Large Numbers, Central Limit Theorem]
---

<img src="https://cdn.britannica.com/80/129380-131-A57EAB51.jpg" style="zoom: 50%;" />

## 公告

期末考結束之後，I2P 告一段落。原訂 1/9 1310-1400 Google Meet 上課
**改以 podcast 形式**於 1/10, (三) 早上於 [**YesKno時間**](https://chtsao.gitlab.io/pmod/about/boardcast/) 外掛上架播出。敬請期待。
我們會談談關於 $\bar{X} = \frac{1}{n} \sum_{i=1}^n X_i$ 的 asymptotics:

#### Shiny R demo
* [Central Limit Theorem](https://rstudio-pubs-static.s3.amazonaws.com/372701_cd54f044df8048d8acaa573420b89e6b.html#/1)
* [Law of Large Numbers](https://rstudio-pubs-static.s3.amazonaws.com/75913_b631566e879449a7b4b9387d3c3fa610.html#/4)


### Asymptotics

這個看起來很偉大的字，在機率與統計中是指  與 樣本數 $n \rightarrow \infty$ 的一些探討, also known as asymptotic analysis, is the describing and study of limiting behaviours of the math/prob/stat objects, for example, $\bar{X}.$  兩個最重要而在理論與應用領域都使用廣泛的定理就是 大數法則 (Law of Large Numbers)   與 中央極限定理 (Central Limit Theorem)。各位可以先在網路或課本上找找他們，讀讀相關的內容，先有個初步的了解，我之後再上傳進一步的解說。

* Text: Chapter 13, Chapter 14

* You may ask the AI-Bots at [⚔️  Chatbot Arena ⚔️ : Benchmarking LLMs in the Wild](https://chat.lmsys.org/) the following questions:

  * Q1: Explain "Law of Large Numbers". Give me some examples and applications. 
  * Q1': 大數法則是什麼？
  * Q2: What is Central Limit Theorem? How does it relate to Binomial probability calculation?
  * Q2': 中央極限定理是什麼？當樣本數很大時，它如何幫助我們計算 binomial probabilities?


 **Kno's Question**: 就這四個問題的答案，其實已有不太正確的部份。對比課本的說明，你可以抓出這些 Aibot 回答中的錯誤嗎？
