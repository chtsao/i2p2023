---
title: "Week 16. Continuous Random Vector"
date: 2023-12-21T21:43:43+08:00
draft: false
tags: [continuous random vector, joint pdf, joint distribution function, marginal pdf, marginal distribution function]
---
<img src="https://cdn.openart.ai/stable_diffusion/dc12e57647efdaeeca28f1b536184ad1d282329d_2000x2000.webp" style="zoom:80%;" />

### Final Exam

- 日期: 2024 0103 (二)
- 時間: 1310-1440.
- 地點:
- 範圍：上課及習題內容。
- 其他：No cheatsheet nor mobile phone. Prepare early and Good Luck!

提早準備，固實會的，加強生疏的，弄懂原來不會的！—-考試不難，會就簡單！

### Basics

Similar to the a discrete random vector, to define or characterize a continuous random vector , it suffices to write down its joint pdf $f(\mathbf{x})$ or joint distribution function $F(\mathbf{x}).$ 

For easy exposition, we consider the case $\mathbf{X} = (X, Y).$ Consider the following three joint pdf 

1. $f(x, y) = c$ if $x, y \in [0, 1]$; $0$, otherwise. 
2. $f(x, y) = cxy$ if $x, y \in [0, 1]$; $0$, otherwise. 
3. $f(x, y) = c (x+y)$ if $x, y \in [0, 1]$; $0$, otherwise. 
4. (For the curious) Substitue the condition $x, y \in [0, 1]$ by $0 \leq x < y \leq 1$ in 1, 2, 3. 

For these pdf's, respectively 

* Find $c$ such that $f$ defines a joint pdf (if possible).
* Compute/Derive $f_X, f_Y, F_X, F_Y$the marginal distribution functions (df), marginal pdf  of $X, Y$ respectively.
* Compute a. $P(0 \leq X \leq 1/2)$. b. $P(X^2 <2, Y^2 \leq \frac{1}{4}).$ ($P(|X-Y| \leq 1)$)
* Compute $\rho_{X, Y} = \frac{Cov(X,Y)}{\sqrt{Var(X) Var(Y)}}$, the correlation between $X$ and $Y$.
* Are $X$ and $Y$ independent?