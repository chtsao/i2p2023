---
title: "Supplemental: Monty Hall Problem and Secretary Problem"
date: 2023-11-01T12:55:17+08:00
draft: false
---

### Formula/Formulation, Answer/Models and Solutions

### Monty Hall Problem
令 $W$ 表示選擇換門而贏得獎品(車)的事件; $R$ 表示(一開始)選擇到獎品那個門的事件; $R^c$ 表示沒有選到獎品門的事件。由條件機率計算可知

$$P(W)=P(W \cap R)+ P(W \cap R^c)=P(W|R)P(R) + P(W|R^c)P(R^c)$$
$$ \hspace{1cm} =0 \times 1/3 + 1 \times 2/3 = 2/3 > 1/3 = P(W^c).$$
也就是換後贏得獎品的機率 2/3 比換後沒有贏得獎品 1/3＝ 1-2/3 的機率來得大。所以應該要選擇換門。

### 參考資料
* Exercise 3.14@ Section 3.6 in [Dekking, et al (2005)](https://link.springer.com/book/10.1007/1-84628-168-7)
* Section 1.3 Cars and Goats: the Monty Hall dilemma
* [實做/模擬](https://www.mathwarehouse.com/monty-hall-simulation-online/)
* [The Secretary Problem @ Libretexts](https://stats.libretexts.org/Bookshelves/Probability_Theory/Probability_Mathematical_Statistics_and_Stochastic_Processes_(Siegrist)/12%3A_Finite_Sampling_Models/12.09%3A_The_Secretary_Problem)