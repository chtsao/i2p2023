---
title: "About"
date: 2023-09-04T09:00:18+08:00
draft: false
categories: [adm]
---
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ff4.bcbits.com%2Fimg%2Fa3083993486_5.jpg&f=1&nofb=1" alt="Uncertainty" style="zoom:33%;" />


* [Syllabus](https://chtsao.gitlab.io/i2p2022/i2p22.sylla.pdf).    Curator: C. Andy (Kno) Tsao  Office: SE A411.  Tel: 3520
* Lectures: Tue. 1310-1500, Thr. 1610-1700 
  * 3D: @ A210.; 
  * [Google ClassRoom](https://classroom.google.com/c/NjI1NzMyNjQ5MjU0?cjc=mbtjchd)
* Office Hours:  Mon 1410-1500; Thr 15:10-16:00 @ SE A411 or by appointment.
* TA Office Hours: 
	* 張秝穎 (二) 1500～1600 @SE A412  611111102@gms.ndhu.edu.tw
	* 李錦州 (三) 1300～1400 @SE A408  611211104@gms.ndhu.edu.tw
* [數學小天地 (NEW) @理 A307 教室](http://faculty.ndhu.edu.tw/~yltseng/help.html)
* Prerequisites: Calculus and curiosity about uncertainty/randomness
* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)
